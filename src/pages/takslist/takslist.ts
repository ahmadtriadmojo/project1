import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ItemSliding } from 'ionic-angular';

/**
 * Generated class for the TakslistPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-takslist',
  templateUrl: 'takslist.html',
})
export class TakslistPage {
  task:array<any> = [];

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.task = [
      {tittle: 'nasi 'goreng', status: 'open'},
      {tittle: 'nasi padang', status: 'open'},
      {tittle: 'nasi kuning', status: 'open'},
      {tittle: 'nasi bungkus', status: 'open'},
      {tittle: 'nasi rames', status: 'open'},
    ]
  }

  addIcon(){
    let theNewTask: string = prompt("New Task");
    if (TheNewTask!=''){
      this.tasks.push({title: theNewTask, status: 'open'})
    }
  }
  markAsDone(slidingItem: ItemSliding, task:any){
    task.status = 'done';
    slidingItem.close();
  }

  removeTask(slidingItem: ItemSliding, task){
    task.status='removed';
    let index = this.tasks.indexOf(task);
    if (index>-1){
      this.tasks.splice(index,1);
    }
    slidingItem.close();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TakslistPage');
  }

}