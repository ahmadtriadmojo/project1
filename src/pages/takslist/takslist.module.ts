import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TakslistPage } from './takslist';

@NgModule({
  declarations: [
    TakslistPage,
  ],
  imports: [
    IonicPageModule.forChild(TakslistPage),
  ],
],
})
export class TakslistpageModule {}